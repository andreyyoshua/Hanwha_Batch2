﻿namespace Sales.Illustration.Data.Domain
{
    public class TopUpWithdrawal
    {
        public bool IsTopup { get; set; }
        public int Year { get; set; }
        public decimal Amount { get; set; }
    }
}
