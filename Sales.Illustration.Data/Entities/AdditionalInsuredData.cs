﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class AdditionalInsuredData
    {
        public AdditionalInsuredData()
        {
            this.AdditionalInsuredRiderDatas = new HashSet<AdditionalInsuredRiderData>();
        }

        [Key]
        public long AdditionalInsuredDataId { get; set; }
        public string TransCode { get; set; }
        public string AdditionalInsuredName { get; set; }
        public Nullable<System.DateTime> AdditionalInsuredDOB { get; set; }
        public Nullable<bool> AdditionalInsuredGender { get; set; }
        public Nullable<int> AdditionalInsuredRiskClassId { get; set; }
        public Nullable<int> AdditionalInsuredAge { get; set; }

        public virtual TransLog TransLog { get; set; }
        public virtual ICollection<AdditionalInsuredRiderData> AdditionalInsuredRiderDatas { get; set; }
    }
}
