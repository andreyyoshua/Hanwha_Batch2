USE [Hanwha_SalesIllustration]
GO
/****** Object:  Table [dbo].[AdditionalInsuredData]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdditionalInsuredData](
	[AdditionalInsuredDataId] [bigint] IDENTITY(1,1) NOT NULL,
	[TransCode] [varchar](30) NULL,
	[AdditionalInsuredName] [varchar](100) NULL,
	[AdditionalInsuredDOB] [datetime] NULL,
	[AdditionalInsuredGender] [char](10) NULL,
	[AdditionalInsuredRiskClassId] [int] NULL,
	[AdditionalInsuredAge] [int] NULL,
	[AdditionalInsuredRelation] [varchar](50) NULL,
 CONSTRAINT [PK_AdditionalInsuredData] PRIMARY KEY CLUSTERED 
(
	[AdditionalInsuredDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdditionalInsuredRiderData]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdditionalInsuredRiderData](
	[AdditionalInsuredRiderDataId] [bigint] IDENTITY(1,1) NOT NULL,
	[AdditionalInsuredDataId] [bigint] NULL,
	[RiderCode] [varchar](10) NULL,
	[RiderTypeId] [int] NULL,
	[Unit] [int] NULL,
	[SumInsured] [money] NULL,
	[AllocationPerc] [float] NULL,
	[COR] [money] NULL,
 CONSTRAINT [PK_AdditionalInsuredRiderData] PRIMARY KEY CLUSTERED 
(
	[AdditionalInsuredRiderDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AnnuityFactor]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AnnuityFactor](
	[AnnuityFactorId] [int] IDENTITY(1,1) NOT NULL,
	[Year] [int] NULL,
	[Factor] [float] NULL,
	[Rate] [float] NULL,
	[ProductType] [varchar](3) NULL,
	[ProductCategory] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_AnnuityFactor] PRIMARY KEY CLUSTERED 
(
	[AnnuityFactorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CashValueRate]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CashValueRate](
	[CashValueRateId] [bigint] IDENTITY(1,1) NOT NULL,
	[Year] [int] NULL,
	[InsuredAge] [int] NULL,
	[ChildAge] [int] NULL,
	[PaymentTerm] [int] NULL,
	[Reserve] [decimal](18, 2) NULL,
	[CashValue] [decimal](18, 2) NULL,
	[CashValueAfterDeath] [decimal](18, 2) NULL,
	[Type] [varchar](10) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_CashValueRate] PRIMARY KEY CLUSTERED 
(
	[CashValueRateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrCode] [varchar](3) NOT NULL,
	[CurrName] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Fund]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fund](
	[FundCode] [varchar](5) NOT NULL,
	[FundName] [varchar](50) NULL,
	[LowRate] [float] NULL,
	[MediumRate] [float] NULL,
	[HighRate] [float] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Fund] PRIMARY KEY CLUSTERED 
(
	[FundCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InvestmentData]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InvestmentData](
	[InvestmentDataId] [bigint] IDENTITY(1,1) NOT NULL,
	[TransCode] [varchar](30) NULL,
	[FundCode] [varchar](5) NULL,
	[FundValue] [int] NULL,
 CONSTRAINT [PK_InvestmentData] PRIMARY KEY CLUSTERED 
(
	[InvestmentDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Language]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Language](
	[CultureId] [varchar](5) NOT NULL,
	[Name] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED 
(
	[CultureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Log]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Log](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[AgentCode] [varchar](100) NULL,
	[AgentName] [varchar](100) NULL,
	[LoginTime] [datetime] NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NasabahData]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NasabahData](
	[NasabahDataId] [bigint] IDENTITY(1,1) NOT NULL,
	[TransCode] [varchar](30) NULL,
	[PolicyHolderName] [varchar](100) NULL,
	[PolicyHolderDOB] [datetime] NULL,
	[PolicyHolderGender] [char](1) NULL,
	[PolicyHolderRiskClassId] [int] NULL,
	[PolicyHolderAge] [int] NULL,
	[IsMainInsured] [bit] NULL,
	[InsuredName] [varchar](100) NULL,
	[InsuredDOB] [datetime] NULL,
	[InsuredGender] [char](1) NULL,
	[InsuredRiskClassId] [int] NULL,
	[InsuredAge] [int] NULL,
	[Relation] [varchar](50) NULL,
	[ChildName] [varchar](100) NULL,
	[ChildDOB] [datetime] NULL,
	[ChildGender] [char](1) NULL,
	[ChildAge] [int] NULL,
	[ProductCode] [varchar](10) NULL,
	[Language] [varchar](5) NULL,
 CONSTRAINT [PK_NasabahData] PRIMARY KEY CLUSTERED 
(
	[NasabahDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentMethod]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentMethod](
	[PMCode] [int] NOT NULL,
	[PMName] [varchar](20) NULL,
	[Factor] [float] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_PaymentMethod] PRIMARY KEY CLUSTERED 
(
	[PMCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PremiumData]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PremiumData](
	[PremiumDataId] [bigint] IDENTITY(1,1) NOT NULL,
	[TransCode] [varchar](30) NULL,
	[Currency] [varchar](3) NULL,
	[PaymentMethod] [int] NULL,
	[PaymentPeriod] [int] NULL,
	[RegularPremium] [money] NULL,
	[RegularTopUp] [money] NULL,
	[SumInsured] [money] NULL,
	[CostOfInsurance] [money] NULL,
	[PaymentPeriodOption] [int] NULL,
	[PaymentMode] [varchar](10) NULL,
	[InsurancePeriod] [int] NULL,
	[UnitWizer] [int] NULL,
	[RatioRegularPremium] [float] NULL,
 CONSTRAINT [PK_PremiumData] PRIMARY KEY CLUSTERED 
(
	[PremiumDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[ProductCode] [varchar](10) NOT NULL,
	[ProductName] [varchar](50) NULL,
	[ProductParent] [varchar](5) NULL,
	[ProductType] [varchar](3) NULL,
	[ProductCategory] [varchar](10) NULL,
	[PolicyHolderMinAge] [int] NULL,
	[PolicyHolderMaxAge] [int] NULL,
	[InsuredMinAge] [int] NULL,
	[InsuredMaxAge] [int] NULL,
	[CovAge] [int] NULL,
	[AdminFee] [money] NULL,
	[MinBasicPremium] [money] NULL,
	[MinRegularTopUp] [money] NULL,
	[RegularTopUpRate] [float] NULL,
	[MinSingleTopUp] [money] NULL,
	[MinWithdrawal] [money] NULL,
	[MinWithdrawalBalance] [money] NULL,
	[MinPaymentYear] [int] NULL,
	[MinPercSumInsured] [int] NULL,
	[MinAmountSumInsured] [money] NULL,
	[MinRemainingBalance] [money] NULL,
	[NegativeYearValidation] [int] NULL,
	[NegativeAgeValidation] [int] NULL,
	[IsActive] [bit] NULL,
	[HasAdditionalInsured] [bit] NULL,
	[TopUpWithdrawalStartYear] [int] NULL,
	[TopUpStartYear] [int] NULL,
	[WithdrawalStartYear] [int] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductAllocationRate]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductAllocationRate](
	[ProductAllocationRateId] [int] IDENTITY(1,1) NOT NULL,
	[ProductCode] [varchar](10) NOT NULL,
	[Year] [int] NULL,
	[RatePremi] [float] NULL,
	[RateSingleTopUp] [float] NULL,
	[RateWithdrawal] [float] NULL,
	[RateDeathBenefit] [float] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ProductAllocationRate] PRIMARY KEY CLUSTERED 
(
	[ProductAllocationRateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCurrency]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCurrency](
	[ProductCurrId] [int] IDENTITY(1,1) NOT NULL,
	[ProductCode] [varchar](10) NOT NULL,
	[CurrCode] [varchar](3) NOT NULL,
 CONSTRAINT [PK_ProductCurrency] PRIMARY KEY CLUSTERED 
(
	[ProductCurrId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductFund]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductFund](
	[ProductFundId] [int] IDENTITY(1,1) NOT NULL,
	[ProductCode] [varchar](10) NOT NULL,
	[FundCode] [varchar](5) NOT NULL,
 CONSTRAINT [PK_ProductFund] PRIMARY KEY CLUSTERED 
(
	[ProductFundId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductPaymentMethodPremium]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductPaymentMethodPremium](
	[ProductPaymentMethodPremiumId] [int] IDENTITY(1,1) NOT NULL,
	[ProductCode] [varchar](10) NULL,
	[PMCode] [int] NULL,
	[MinAmount] [money] NULL,
	[Type] [varchar](5) NULL,
 CONSTRAINT [PK_ProductPaymentMethodPremium] PRIMARY KEY CLUSTERED 
(
	[ProductPaymentMethodPremiumId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductRate]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductRate](
	[ProductRateId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductCode] [varchar](10) NULL,
	[Age] [int] NULL,
	[Rate] [float] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ProductRate] PRIMARY KEY CLUSTERED 
(
	[ProductRateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductTahapanRate]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductTahapanRate](
	[ProductTahapanRateId] [int] IDENTITY(1,1) NOT NULL,
	[ProductCode] [varchar](10) NULL,
	[Year] [int] NULL,
	[RateTahapan] [float] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ProductTahapanRate] PRIMARY KEY CLUSTERED 
(
	[ProductTahapanRateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReportTemplate]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReportTemplate](
	[ReportTemplateId] [int] IDENTITY(1,1) NOT NULL,
	[ProductCode] [varchar](10) NULL,
	[CultureId] [varchar](5) NULL,
	[ReportName] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ReportTemplate] PRIMARY KEY CLUSTERED 
(
	[ReportTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rider]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rider](
	[RiderCode] [varchar](10) NOT NULL,
	[RiderName] [varchar](80) NULL,
	[RiderDesc] [varchar](80) NULL,
	[PolicyHolderMinAge] [int] NULL,
	[PolicyHolderMaxAge] [int] NULL,
	[InsuredMinAge] [int] NULL,
	[InsuredMaxAge] [int] NULL,
	[CoverTerm] [int] NULL,
	[CoverTermWizer] [int] NULL,
	[HasSumInsured] [bit] NULL,
	[MinPercSumInsured] [float] NULL,
	[MaxPercSumInsured] [float] NULL,
	[MinAmountSumInsured] [money] NULL,
	[MaxAmountSumInsured] [money] NULL,
	[HasType] [bit] NULL,
	[Description] [text] NULL,
	[IsActive] [bit] NULL,
	[Category] [varchar](10) NULL,
	[DescriptionEN] [text] NULL,
	[AllowAdditionalInsured] [varchar](100) NULL,
	[DisableRider] [varchar](50) NULL,
	[ChildInsuredMinAge] [int] NULL,
	[ChildInsuredMaxAge] [int] NULL,
 CONSTRAINT [PK_Rider_1] PRIMARY KEY CLUSTERED 
(
	[RiderCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RiderData]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RiderData](
	[RiderDataId] [bigint] IDENTITY(1,1) NOT NULL,
	[TransCode] [varchar](30) NULL,
	[RiderCode] [varchar](10) NULL,
	[RiderTypeId] [int] NULL,
	[Unit] [int] NULL,
	[SumInsured] [money] NULL,
	[AllocationPerc] [float] NULL,
	[COR] [money] NULL,
 CONSTRAINT [PK_RiderData] PRIMARY KEY CLUSTERED 
(
	[RiderDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RiderProduct]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RiderProduct](
	[RiderProductId] [int] IDENTITY(1,1) NOT NULL,
	[RiderCode] [varchar](10) NULL,
	[ProductCode] [varchar](10) NULL,
	[RiderTypeId] [int] NULL,
	[CoverTerm] [int] NULL,
 CONSTRAINT [PK_RiderProduct] PRIMARY KEY CLUSTERED 
(
	[RiderProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RiderRate]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RiderRate](
	[RiderRateId] [int] IDENTITY(1,1) NOT NULL,
	[RiderCode] [varchar](10) NULL,
	[RiderTypeId] [int] NULL,
	[Age] [int] NULL,
	[Rate] [float] NULL,
	[RiskClass] [int] NULL,
	[Category] [varchar](10) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_RiderRate] PRIMARY KEY CLUSTERED 
(
	[RiderRateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RiderType]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RiderType](
	[RiderTypeId] [int] IDENTITY(1,1) NOT NULL,
	[RiderCode] [varchar](10) NOT NULL,
	[TypeName] [varchar](10) NULL,
	[MinAge] [int] NULL,
	[MaxAge] [int] NULL,
	[CoverTerm] [int] NULL,
	[ParentRiderType] [int] NULL,
	[Description] [text] NULL,
	[DescriptionEN] [text] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_RiderType] PRIMARY KEY CLUSTERED 
(
	[RiderTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RiskClass]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RiskClass](
	[RiskClassId] [int] IDENTITY(1,1) NOT NULL,
	[RiskClassDesc] [varchar](50) NULL,
	[Class] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_RiskClass] PRIMARY KEY CLUSTERED 
(
	[RiskClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SantunanHarian]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SantunanHarian](
	[SantunanHarianId] [int] IDENTITY(1,1) NOT NULL,
	[RiderCode] [varchar](10) NULL,
	[RiderTypeId] [int] NULL,
	[Santunan] [money] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_SantunanHarian] PRIMARY KEY CLUSTERED 
(
	[SantunanHarianId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TopUpWithdrawalData]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TopUpWithdrawalData](
	[TopUpWithdrawalDataId] [bigint] IDENTITY(1,1) NOT NULL,
	[TransCode] [varchar](30) NULL,
	[IsTopUp] [bit] NULL,
	[TransYear] [int] NULL,
	[TransAmount] [money] NULL,
 CONSTRAINT [PK_TopUpWithdrawalData] PRIMARY KEY CLUSTERED 
(
	[TopUpWithdrawalDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TraditionalRate]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TraditionalRate](
	[TraditionalRateId] [bigint] IDENTITY(1,1) NOT NULL,
	[TraditionalRateType] [varchar](10) NULL,
	[ChildAge] [int] NULL,
	[InsuredAge] [int] NULL,
	[PremiumTerm] [int] NULL,
	[InsurancePeriod] [int] NULL,
	[Rate] [float] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_TraditionalRate] PRIMARY KEY CLUSTERED 
(
	[TraditionalRateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TraditionalRiderRate]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TraditionalRiderRate](
	[TraditionalRiderRateId] [bigint] IDENTITY(1,1) NOT NULL,
	[RiderCode] [varchar](10) NULL,
	[RiderTypeId] [int] NULL,
	[TraditionalRateType] [varchar](10) NULL,
	[InsuredAge] [int] NULL,
	[BasicCov] [int] NULL,
	[BasicPremiTerm] [int] NULL,
	[RiderCov] [int] NULL,
	[RiderPremiTerm] [int] NULL,
	[Rate] [float] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_TraditionalRiderRate] PRIMARY KEY CLUSTERED 
(
	[TraditionalRiderRateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransLog]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransLog](
	[TransCode] [varchar](30) NOT NULL,
	[AgentCode] [varchar](10) NULL,
	[TransStatus] [int] NULL,
	[TransDate] [datetime] NULL,
 CONSTRAINT [PK_TransLog] PRIMARY KEY CLUSTERED 
(
	[TransCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UnitMapping]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UnitMapping](
	[UnitMappingId] [int] IDENTITY(1,1) NOT NULL,
	[RiderCode] [varchar](10) NULL,
	[RiderTypeId] [int] NULL,
	[Unit] [int] NULL,
	[SIFrom] [money] NULL,
	[SITo] [money] NULL,
	[MaxBenefit] [money] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_UnitMapping] PRIMARY KEY CLUSTERED 
(
	[UnitMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserMembership]    Script Date: 16-May-17 08:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserMembership](
	[AgentCode] [varchar](10) NOT NULL,
	[AgentName] [varchar](100) NULL,
	[JoinDate] [datetime] NULL,
	[AAJILicense] [varchar](20) NULL,
	[EncPass] [varchar](max) NOT NULL,
	[IsActive] [bit] NULL,
	[ExpiredDate] [datetime] NULL,
	[Email] [varchar](100) NULL,
	[RoleUser] [varchar](20) NULL,
 CONSTRAINT [PK_UserMembership_1] PRIMARY KEY CLUSTERED 
(
	[AgentCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AdditionalInsuredData]  WITH CHECK ADD  CONSTRAINT [FK_AdditionalInsuredData_TransLog] FOREIGN KEY([TransCode])
REFERENCES [dbo].[TransLog] ([TransCode])
GO
ALTER TABLE [dbo].[AdditionalInsuredData] CHECK CONSTRAINT [FK_AdditionalInsuredData_TransLog]
GO
ALTER TABLE [dbo].[AdditionalInsuredRiderData]  WITH CHECK ADD  CONSTRAINT [FK_AdditionalInsuredRiderData_AdditionalInsuredData] FOREIGN KEY([AdditionalInsuredDataId])
REFERENCES [dbo].[AdditionalInsuredData] ([AdditionalInsuredDataId])
GO
ALTER TABLE [dbo].[AdditionalInsuredRiderData] CHECK CONSTRAINT [FK_AdditionalInsuredRiderData_AdditionalInsuredData]
GO
ALTER TABLE [dbo].[InvestmentData]  WITH CHECK ADD  CONSTRAINT [FK_InvestmentData_TransLog] FOREIGN KEY([TransCode])
REFERENCES [dbo].[TransLog] ([TransCode])
GO
ALTER TABLE [dbo].[InvestmentData] CHECK CONSTRAINT [FK_InvestmentData_TransLog]
GO
ALTER TABLE [dbo].[NasabahData]  WITH CHECK ADD  CONSTRAINT [FK_NasabahData_TransLog] FOREIGN KEY([TransCode])
REFERENCES [dbo].[TransLog] ([TransCode])
GO
ALTER TABLE [dbo].[NasabahData] CHECK CONSTRAINT [FK_NasabahData_TransLog]
GO
ALTER TABLE [dbo].[PremiumData]  WITH CHECK ADD  CONSTRAINT [FK_PremiumData_TransLog] FOREIGN KEY([TransCode])
REFERENCES [dbo].[TransLog] ([TransCode])
GO
ALTER TABLE [dbo].[PremiumData] CHECK CONSTRAINT [FK_PremiumData_TransLog]
GO
ALTER TABLE [dbo].[ProductAllocationRate]  WITH CHECK ADD  CONSTRAINT [FK_Product_ProductAllocationRate] FOREIGN KEY([ProductCode])
REFERENCES [dbo].[Product] ([ProductCode])
GO
ALTER TABLE [dbo].[ProductAllocationRate] CHECK CONSTRAINT [FK_Product_ProductAllocationRate]
GO
ALTER TABLE [dbo].[ProductCurrency]  WITH CHECK ADD  CONSTRAINT [FK_Currency_ProductCurrency] FOREIGN KEY([CurrCode])
REFERENCES [dbo].[Currency] ([CurrCode])
GO
ALTER TABLE [dbo].[ProductCurrency] CHECK CONSTRAINT [FK_Currency_ProductCurrency]
GO
ALTER TABLE [dbo].[ProductCurrency]  WITH CHECK ADD  CONSTRAINT [FK_Product_ProductCurrency] FOREIGN KEY([ProductCode])
REFERENCES [dbo].[Product] ([ProductCode])
GO
ALTER TABLE [dbo].[ProductCurrency] CHECK CONSTRAINT [FK_Product_ProductCurrency]
GO
ALTER TABLE [dbo].[ProductFund]  WITH CHECK ADD  CONSTRAINT [FK_Fund_ProductFund] FOREIGN KEY([FundCode])
REFERENCES [dbo].[Fund] ([FundCode])
GO
ALTER TABLE [dbo].[ProductFund] CHECK CONSTRAINT [FK_Fund_ProductFund]
GO
ALTER TABLE [dbo].[ProductFund]  WITH CHECK ADD  CONSTRAINT [FK_Product_ProductFund] FOREIGN KEY([ProductCode])
REFERENCES [dbo].[Product] ([ProductCode])
GO
ALTER TABLE [dbo].[ProductFund] CHECK CONSTRAINT [FK_Product_ProductFund]
GO
ALTER TABLE [dbo].[ProductPaymentMethodPremium]  WITH CHECK ADD  CONSTRAINT [FK_ProductPaymentMethodPremium_PaymentMethod] FOREIGN KEY([PMCode])
REFERENCES [dbo].[PaymentMethod] ([PMCode])
GO
ALTER TABLE [dbo].[ProductPaymentMethodPremium] CHECK CONSTRAINT [FK_ProductPaymentMethodPremium_PaymentMethod]
GO
ALTER TABLE [dbo].[ProductRate]  WITH CHECK ADD  CONSTRAINT [FK_ProductRate_Product] FOREIGN KEY([ProductCode])
REFERENCES [dbo].[Product] ([ProductCode])
GO
ALTER TABLE [dbo].[ProductRate] CHECK CONSTRAINT [FK_ProductRate_Product]
GO
ALTER TABLE [dbo].[ProductTahapanRate]  WITH CHECK ADD  CONSTRAINT [FK_ProductTahapanRate_Product] FOREIGN KEY([ProductCode])
REFERENCES [dbo].[Product] ([ProductCode])
GO
ALTER TABLE [dbo].[ProductTahapanRate] CHECK CONSTRAINT [FK_ProductTahapanRate_Product]
GO
ALTER TABLE [dbo].[ReportTemplate]  WITH CHECK ADD  CONSTRAINT [FK_ReportTemplate_Product] FOREIGN KEY([ProductCode])
REFERENCES [dbo].[Product] ([ProductCode])
GO
ALTER TABLE [dbo].[ReportTemplate] CHECK CONSTRAINT [FK_ReportTemplate_Product]
GO
ALTER TABLE [dbo].[RiderData]  WITH CHECK ADD  CONSTRAINT [FK_RiderData_TransLog] FOREIGN KEY([TransCode])
REFERENCES [dbo].[TransLog] ([TransCode])
GO
ALTER TABLE [dbo].[RiderData] CHECK CONSTRAINT [FK_RiderData_TransLog]
GO
ALTER TABLE [dbo].[RiderProduct]  WITH CHECK ADD  CONSTRAINT [FK_RiderProduct_Rider] FOREIGN KEY([RiderCode])
REFERENCES [dbo].[Rider] ([RiderCode])
GO
ALTER TABLE [dbo].[RiderProduct] CHECK CONSTRAINT [FK_RiderProduct_Rider]
GO
ALTER TABLE [dbo].[RiderProduct]  WITH CHECK ADD  CONSTRAINT [FK_RiderProduct_RiderType] FOREIGN KEY([RiderTypeId])
REFERENCES [dbo].[RiderType] ([RiderTypeId])
GO
ALTER TABLE [dbo].[RiderProduct] CHECK CONSTRAINT [FK_RiderProduct_RiderType]
GO
ALTER TABLE [dbo].[RiderRate]  WITH CHECK ADD  CONSTRAINT [FK_RiderRate_Rider] FOREIGN KEY([RiderCode])
REFERENCES [dbo].[Rider] ([RiderCode])
GO
ALTER TABLE [dbo].[RiderRate] CHECK CONSTRAINT [FK_RiderRate_Rider]
GO
ALTER TABLE [dbo].[RiderRate]  WITH CHECK ADD  CONSTRAINT [FK_RiderRate_RiderType] FOREIGN KEY([RiderTypeId])
REFERENCES [dbo].[RiderType] ([RiderTypeId])
GO
ALTER TABLE [dbo].[RiderRate] CHECK CONSTRAINT [FK_RiderRate_RiderType]
GO
ALTER TABLE [dbo].[RiderType]  WITH CHECK ADD  CONSTRAINT [FK_RiderType_Rider] FOREIGN KEY([RiderCode])
REFERENCES [dbo].[Rider] ([RiderCode])
GO
ALTER TABLE [dbo].[RiderType] CHECK CONSTRAINT [FK_RiderType_Rider]
GO
ALTER TABLE [dbo].[TopUpWithdrawalData]  WITH CHECK ADD  CONSTRAINT [FK_TopUpWithdrawalData_TransLog] FOREIGN KEY([TransCode])
REFERENCES [dbo].[TransLog] ([TransCode])
GO
ALTER TABLE [dbo].[TopUpWithdrawalData] CHECK CONSTRAINT [FK_TopUpWithdrawalData_TransLog]
GO
ALTER TABLE [dbo].[TransLog]  WITH CHECK ADD  CONSTRAINT [FK_TransLog_UserMembership] FOREIGN KEY([AgentCode])
REFERENCES [dbo].[UserMembership] ([AgentCode])
GO
ALTER TABLE [dbo].[TransLog] CHECK CONSTRAINT [FK_TransLog_UserMembership]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'3 digit : reg for reguler, tra for traditional, sin for single' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProductType'
GO
