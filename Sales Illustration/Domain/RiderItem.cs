﻿using Sales.Illustration.Web.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Sales.Illustration.Web.Domain
{
    public class RiderItem
    {
        public Rider Rider { get; set; }

        public bool Checked { get; set; }
        public string BiayaAsuransi { get; set; }
        public string AllocationPercent { get; set; }
        public string UangPertanggungan { get; set; }
        public string UnitName { get; set; }
        public string Unit { get; set; }
        public string Choice { get; set; }
        public List<Choices> Choices { get; set; }
    }

    public class Choices
    {
        public RiderType RiderType { get; set; }
        public bool Checked { get; set; }
        public string BiayaAsuransi { get; set; }
    }
}
