﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sales.Illustration.Web.Helper;

namespace Sales.Illustration.Web.Controllers
{
    public class LogController : Controller
    {
        Sales.Illustration.Web.Models.MainModel obj = new Models.MainModel();
        public class data
        {
            public string monthyear { get; set; }
        }
        public ActionResult Index()
        {
            var monthyear = _session.MonthYear == null || _session.MonthYear == "" ? DateTime.Now.ToString("MM-yyyy") : _session.MonthYear;
            ViewData["logdata"] = obj.GetLogHistory(monthyear);
            ViewData["monthyear"] = monthyear;
            
            return View();
        }

        [HttpPost]
        public ActionResult ValidateMonthYear(data data)
        {
            _session.MonthYear = data.monthyear;
            return RedirectToAction("Index");
        }
    }
}
