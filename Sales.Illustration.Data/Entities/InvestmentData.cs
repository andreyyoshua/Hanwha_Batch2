﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class InvestmentData
    {
        [Key]
        public long InvestmentDataId { get; set; }
        public string TransCode { get; set; }
        public string FundCode { get; set; }
        public Nullable<int> FundValue { get; set; }

        public virtual TransLog TransLog { get; set; }
    }
}
