﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class ProductFund
    {
        [Key]
        public int ProductFundId { get; set; }
        public string ProductCode { get; set; }
        public string FundCode { get; set; }

        public virtual Fund Fund { get; set; }
        public virtual Product Product { get; set; }
    }
}
