﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class PaymentMethod
    {
        public PaymentMethod()
        {
            this.ProductPaymentMethodPremiums = new HashSet<ProductPaymentMethodPremium>();
        }

        [Key]
        public string PMCode { get; set; }
        public string PMName { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual ICollection<ProductPaymentMethodPremium> ProductPaymentMethodPremiums { get; set; }
    }
}
