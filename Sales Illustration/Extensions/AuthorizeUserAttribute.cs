﻿using Sales.Illustration.Web.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Extensions
{
    public class DeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Flush();

            var path = Path.Combine(filterContext.RequestContext.HttpContext.Server.MapPath("~/Report/temp"), _session.TransLogCode + ".pdf");
            var fi = new FileInfo(path);
            if (fi.Exists)
                fi.Delete();
        }
    }
}