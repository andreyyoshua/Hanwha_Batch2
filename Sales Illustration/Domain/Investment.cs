﻿namespace Sales.Illustration.Web.Domain
{
    public class Investment
    {
        public string InvestmentCode { get; set; }
        public string InvestmentName { get; set; }
        public int Percentage { get; set; }
    }
}
