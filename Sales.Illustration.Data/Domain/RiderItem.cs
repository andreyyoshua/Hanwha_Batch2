﻿using Sales.Illustration.Data.Entities;
using System.Collections.Generic;
using System.Web.Mvc;
namespace Sales.Illustration.Data.Domain
{
    public class RiderItem
    {
        public Rider Rider { get; set; }
        public bool Checked { get; set; }
        public decimal? BiayaAsuransi { get; set; }
        public int? AllocationPercent { get; set; }
        public decimal? UangPertanggungan { get; set; }
    }
}
