﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class Fund
    {
        public Fund()
        {
            this.ProductFunds = new HashSet<ProductFund>();
        }

        [Key]
        public string FundCode { get; set; }
        public string FundName { get; set; }
        public Nullable<double> LowRate { get; set; }
        public Nullable<double> MediumRate { get; set; }
        public Nullable<double> HighRate { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual ICollection<ProductFund> ProductFunds { get; set; }
    }
}
