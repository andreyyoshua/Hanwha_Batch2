﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class Rider
    {
        public Rider()
        {
            this.RiderProducts = new HashSet<RiderProduct>();
            this.RiderRates = new HashSet<RiderRate>();
            this.RiderTypes = new HashSet<RiderType>();
        }

        [Key]
        public int RiderId { get; set; }
        public string RiderCode { get; set; }
        public string RiderName { get; set; }
        public Nullable<int> OrderNo { get; set; }
        public Nullable<int> PolicyHolderMinAge { get; set; }
        public Nullable<int> PolicyHolderMaxAge { get; set; }
        public Nullable<int> InsuredMinAge { get; set; }
        public Nullable<int> InsuredMaxAge { get; set; }
        public Nullable<int> CoverTerm { get; set; }
        public Nullable<bool> HasSumInsured { get; set; }
        public Nullable<double> MinPercSumInsured { get; set; }
        public Nullable<double> MaxPercSumInsured { get; set; }
        public Nullable<decimal> MinAmountSumInsured { get; set; }
        public Nullable<decimal> MaxAmountSumInsured { get; set; }
        public Nullable<bool> HasType { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual ICollection<RiderProduct> RiderProducts { get; set; }
        public virtual ICollection<RiderRate> RiderRates { get; set; }
        public virtual ICollection<RiderType> RiderTypes { get; set; }
    }
}
