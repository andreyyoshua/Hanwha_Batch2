﻿namespace Sales.Illustration.Data.Domain
{
    public class Investment
    {
        public int InvestmentCode { get; set; }
        public string InvestmentName { get; set; }
        public int Percentage { get; set; }
    }
}
