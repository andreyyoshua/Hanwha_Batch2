using Sales.Illustration.Data.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Sales.Illustration.Data
{
    public class HanwhaContext : DbContext
    {
        // Your context has been configured to use a 'HanwhaContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Sales.Illustration.Data.HanwhaContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'HanwhaContext' 
        // connection string in the application configuration file.
        public HanwhaContext()
            : base("HanwhaContext")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }

        public virtual DbSet<AdditionalInsuredData> AdditionalInsuredDatas { get; set; }
        public virtual DbSet<AdditionalInsuredRiderData> AdditionalInsuredRiderDatas { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<Fund> Funds { get; set; }
        public virtual DbSet<InvestmentData> InvestmentDatas { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<NasabahData> NasabahDatas { get; set; }
        public virtual DbSet<PaymentMethod> PaymentMethods { get; set; }
        public virtual DbSet<PremiumData> PremiumDatas { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductAllocationRate> ProductAllocationRates { get; set; }
        public virtual DbSet<ProductCurrency> ProductCurrencies { get; set; }
        public virtual DbSet<ProductFund> ProductFunds { get; set; }
        public virtual DbSet<ProductPaymentMethodPremium> ProductPaymentMethodPremiums { get; set; }
        public virtual DbSet<Rider> Riders { get; set; }
        public virtual DbSet<RiderData> RiderDatas { get; set; }
        public virtual DbSet<RiderProduct> RiderProducts { get; set; }
        public virtual DbSet<RiderRate> RiderRates { get; set; }
        public virtual DbSet<RiderType> RiderTypes { get; set; }
        public virtual DbSet<RiskClass> RiskClasses { get; set; }
        public virtual DbSet<SummaryData> SummaryDatas { get; set; }
        public virtual DbSet<TopUpWithdrawalData> TopUpWithdrawalDatas { get; set; }
        public virtual DbSet<TransLog> TransLogs { get; set; }
        public virtual DbSet<UserMembership> UserMemberships { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions
                .Remove<PluralizingTableNameConvention>();
        }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}