﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Sales.Illustration.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;

            bundles.Add(new StyleBundle("~/Content/pluginscss").Include(
                        "~/Content/plugins/css/bootstrap.css",
                        "~/Content/plugins/css/bootstrap.font.css",
                        "~/Content/plugins/css/custom.css",
                        "~/Content/plugins/css/custom.portal.css",
                        "~/Content/dist/css/bootstrap.min.css",
                        "~/Content/dist/css/bootstrap-datetimepicker.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/pluginsjs").Include(
                        "~/Content/plugins/js/jquery.js",
                        "~/Content/plugins/js/jquery-ui.js",
                        "~/Content/plugins/js/bootstrap.js",
                        "~/Content/plugins/js/datepicker.js",
                        "~/Content/plugins/js/custom.js",
                        "~/Content/plugins/js/angular.min.js",
                        "~/Script/alertify.js",
                        "~/Script/picker.js",
                        "~/Script/picker.date.js",
                        "~/Script/picker.time.js",
                        "~/Script/legacy.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/dist/css/font-awesome/css/font-awesome.min.css",
                        "~/Content/dist/css/hanwha.css",
                        "~/Content/dist/css/data-calon-nasabah.css",
                        "~/Content/dist/css/simple-sidebar.css",
                        "~/Content/dist/css/alertify.css",
                        "~/Content/dist/css/alertify.default.css",
                        "~/Content/dist/css/datepicker.default.css",
                        "~/Content/dist/css/datepicker.default.date.css"));

            bundles.Add(new ScriptBundle("~/bundles/distjs").Include(
                        "~/Content/dist/js/bootstrap.min.js",
                        "~/Content/dist/js/moment.js",
                        "~/Content/dist/js/bootstrap-datetimepicker.min.js",
                        "~/Content/dist/js/danny.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include("~/Script/app.js"));
        }
    }
}