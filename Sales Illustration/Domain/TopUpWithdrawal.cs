﻿namespace Sales.Illustration.Web.Domain
{
    public class TopUpWithdrawal
    {
        public bool IsTopUp { get; set; } // Whether TOPUP or WITHDRAWAL
        public int Year { get; set; }
        public string Amount { get; set; }
    }
}
