﻿namespace Sales.Illustration.Data.Domain
{
    public class LoginDataResult
    {
        public string status_login { get; set; }
        public string ag_code { get; set; }
        public string ag_name { get; set; }
        public string aaji_license { get; set; }
        public string join_date { get; set; }
        public string enc_pass { get; set; }
    }
}
